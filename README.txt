-- INTRODUCTION --

Datadome modules provides integration with the DataDome Bot Mitigation Service 
(https://datadome.co/).
It currently supports automated creation of the DataDome JavaScript tag 
https://docs.datadome.co/docs/javascript-tag and logging of access events 
by bots and other undesirable clients.

Please get the datadome JS key from datadome and store in drupal variable 
'datadome_js_key' to make this work and integrate properly with datadome

-- REQUIREMENTS --

 - Datadome JS Key -  This can be obtained by creating account in Datadome 
and get from there.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure the drupal variable ( datadome_js_key ) via settings.php
    - $conf['datadome_js_key'] = 'sdsaa11asSDADSADFHGS';
